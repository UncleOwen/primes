# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring

import pytest
from hypothesis import given
from hypothesis.strategies import integers

from primes import is_prime


def test_one_is_not_prime() -> None:
    assert not is_prime(1)


@pytest.mark.parametrize('prime', [
    # single digit primes
    2, 3, 5, 7,
    # 10th, 100th, 1000th, 10000th and 100000th prime according to
    # https://oeis.org/A000040/a000040.txt
    29, 541, 7919, 104729, 1299709,
    # largest prime smaller than 2**n for n \in {8, 16, 32}
    # according to https://primes.utm.edu/lists/2small/0bit.html
    2**2**3-5, 2**2**4-15, 2**2**5-5,
    # Fermat Prime Numbers
    2**2**2+1, 2**2**3+1, 2**2**4+1,
])
def test_primes_are_primes(prime: int) -> None:
    assert is_prime(prime)


@given(
    integers(min_value=2),
    integers(min_value=2, max_value=500_000),
    # In theory, the max value in not necessary.
    # In practice, the max value prevents us solving the
    # integer factorization problem - which is HARD.
)
def test_composites_are_not_prime(factor1: int, factor2: int) -> None:
    assert not is_prime(factor1*factor2)


@given(integers(min_value=2, max_value=500_000))
def test_squares_are_not_prime(factor: int) -> None:
    # This is a special case of composites_are_not_prime.
    # It is included here, because missing squares of primes
    # is a common mistake when implementing is_prime()
    assert not is_prime(factor*factor)
