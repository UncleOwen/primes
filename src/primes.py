'''
This module is all about prime numbers.

Currently the only function is is_prime
'''

from itertools import count
from locale import setlocale, LC_ALL
try:
    # https://github.com/python/mypy/issues/1393
    from math import isqrt   # type: ignore[attr-defined]
except ImportError:
    from gmpy2 import isqrt  # type: ignore[import,no-redef]
from time import perf_counter


def is_prime(candidate: int) -> bool:
    'check if a number is a prime number'

    if candidate == 1:
        return False

    square_root = isqrt(candidate)
    for divisor in range(2, square_root+1):
        if candidate % divisor == 0:
            return False
    return True


def main() -> None:
    'time trial for is_prime'

    setlocale(LC_ALL, '')

    for limit in (
        i * 10**j
        for j in count(2)
        for i in [2, 3, 5, 8, 12]
        # exponential back-off while still producing nice-looking numbers
    ):
        start = perf_counter()
        for i in range(limit):
            is_prime(i)
        end = perf_counter()
        duration = end - start
        if duration > 2:
            print(f'Tested {limit:n} numbers for primality in {duration:.2f}s')
            return


if __name__ == '__main__':
    main()
